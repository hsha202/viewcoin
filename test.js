
var request = require('request');
var q = require('q');

var getTicker = function(url) {
    var deferred = q.defer();
    request(url, function(error, response, body) {
        if(error) {
            deferred.reject(error);
        } else {
            deferred.resolve(body);
        }
    });
    return deferred.promise;
};


getTicker("https://api.bithumb.com/public/recent_transactions/BTC").then(function(data) {
    var bodyData = JSON.parse(data);
    console.log(bodyData.data[0]);

});