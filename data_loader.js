var request = require('request');
var q = require('q');

var currencies = ["BTC", "ETH"];
var traders = ["Bithumb", "Korbit", "CoinOne"];
var coinData = {};
//init coinData
for(var i in currencies) {
    var currency = currencies[i];
    for( var j in traders) {
        var trader = traders[j];
        coinData[currency] = {};
        coinData[currency][trader] = {};
    }
}

var interval = 13 * 1000; // 15 sec
exports.getCoinData = function() {
    return coinData;
};

// private functions
/*
 *  1. Common functions
 */
setTimeout(function() {
    updateBithumbData();
    updateKorbitData();
    updateCoinone();
},0);

setInterval(function() {
    updateBithumbData();
    updateKorbitData();
    updateCoinone();
}, interval);

var makeCoinData = function(price, maxPrice, minPrice, volume1Day, changePrice, changePercent) {
    return {
        price : price,
        max_price : maxPrice,
        min_price : minPrice,
        volume_1day : volume1Day,
        change_price : changePrice,
        change_percent : changePercent
    };
};

var setCoinPrice = function(currency, traders, name, data) {
    data.name = name;
    coinData[currency] = coinData[currency] || {};
    coinData[currency][traders] = data;
};

var getTicker = function(url) {
    var deferred = q.defer();
    request(url, function(error, response, body) {
        if(error) {
            deferred.reject(error);
        } else {
            deferred.resolve(body);
        }
    });
    return deferred.promise;
};

/*
 *  2. Bithumb
 */
var updateBithumbData = function() {
    getTicker("https://api.bithumb.com/public/ticker/ALL").then(function(data) {
        var bodyData = JSON.parse(data).data;
        for(var i in currencies) {
            var currency = currencies[i];
            setCoinPrice(currency, "Bithumb", "빗썸", makeCoinData(
                bodyData[currency].closing_price,
                bodyData[currency].max_price,
                bodyData[currency].min_price,
                (bodyData[currency].volume_1day*1).toFixed(0),
                bodyData[currency].closing_price - bodyData[currency].opening_price,
                (((bodyData[currency].closing_price - bodyData[currency].opening_price)/ bodyData[currency].opening_price)*100).toFixed(2)
            ));
        }
    });
};

/*
 *  3. Korbit
 */
var updateKorbitData = function() {
    getTicker("https://api.korbit.co.kr/v1/ticker/detailed?currency_pair=btc_krw").then(function(bodyData) {
        var data = JSON.parse(bodyData);
        var yesterdayPrice = (((data.high*1)+(data.low*1))/2).toFixed(0);
        var change = data.last-yesterdayPrice;
        var changePercent = (change/yesterdayPrice*100).toFixed(2);
        setCoinPrice("BTC", "Korbit", "코빗", makeCoinData(
            data.last,
            data.high,
            data.low,
            (data.volume*1).toFixed(0),
            change,
            changePercent
        ));
    });

    getTicker("https://api.korbit.co.kr/v1/ticker/detailed?currency_pair=eth_krw").then(function(bodyData) {
        var data = JSON.parse(bodyData);
        var yesterdayPrice = (((data.high*1)+(data.low*1))/2).toFixed(0);
        var change = data.last-yesterdayPrice;
        var changePercent = (change/yesterdayPrice*100).toFixed(2);
        setCoinPrice("ETH", "Korbit", "코빗", makeCoinData(
            data.last,
            data.high,
            data.low,
            (data.volume*1).toFixed(0),
            change,
            changePercent
        ));
    });
};

/*
 *  4. Coin one
 */
var updateCoinone = function() {
    getTicker("https://api.coinone.co.kr/ticker/?currency=all").then(function(bodyData) {
        var data = JSON.parse(bodyData);
        for(var i in currencies) {
            var currency = currencies[i];
            var lowerCase = currency.toLowerCase();

            setCoinPrice(currency, "CoinOne", "코인원", makeCoinData(
                data[lowerCase].last,
                data[lowerCase].high,
                data[lowerCase].low,
                (data[lowerCase].volume*1).toFixed(0),
                data[lowerCase].last - data[lowerCase].first,
                (((data[lowerCase].last-data[lowerCase].first)/data[lowerCase].first)*100).toFixed(2)
            ));
        }
    });
};


