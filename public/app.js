(function() {
    "use strict";

    var app = angular.module("vc", []);

    app.service("MainService", function($q, $http, $interval, $rootScope) {
        var vm = this;

        var getTicker = function(url) {
            var defer = $q.defer();
            $http({
                method:'GET',
                url:url
            }).then(function(data) {
                defer.resolve(data);
            }, function(response) {
                defer.reject(response);
            });
            return defer.promise;
        };

        vm.getKoreaData = function() {
            return getTicker('/api/all');
        };

        vm.getPoloniex = function() {
            return getTicker('https://poloniex.com/public?command=returnTicker');
        };
    });

    app.constant("CONST", {
        INTERVAL:15
    });

    app.controller("RootController", function($rootScope, $interval, MainService, CONST) {
        $rootScope.remainTime = 15;
        $rootScope.interval = CONST.INTERVAL;
        $rootScope.lastUpdated = (new Date()).toLocaleDateString();

        var timeCount = CONST.INTERVAL;
        getKoreaData();
        getPoloniexData();
        $interval(interval, 1000);

        function interval() {
            timeCount++;
            $rootScope.remainTime = CONST.INTERVAL-((timeCount % CONST.INTERVAL)+1);
            if( $rootScope.remainTime === 0 ) {
                getKoreaData();
                getPoloniexData();
            }
        }

        // for koreaData
        function getKoreaData() {
            MainService.getKoreaData().then(function (data) {
                $rootScope.koreaData = data.data;
            }, function (res) {
                console.log(res);
            });
        }

        // for poloniexData
        $rootScope.poloniexData = {};
        var marketTypes = ["BTC", "USDT", "XMR", "ETH"];
        function initPoloniexData() {
            for(var i in marketTypes) {
                $rootScope.poloniexData[marketTypes[i]] = [];
                $rootScope.poloniexData[marketTypes[i]].name = marketTypes[i];
            }
        }
        function getPoloniexData() {
            MainService.getPoloniex().then(function(data) {
                initPoloniexData();
                for(var i in data.data) {
                    var name = i.split("_");
                    var item = data.data[i];
                    item.name = name[1];
                    $rootScope.poloniexData[name[0]].push(item);
                }
            },function(res){
                console.log(res);
            });
        }
    });

    app.controller("HeadContoller", function() {
        var vm = this;
        var bodyEl = $("body");

        vm.goToTop= function() {
            bodyEl.animate({scrollTop:0}, 400);
        };
        vm.scrollTo = function(id) {
            var offsetTop = $("#"+id).offset();
            var headHeight = $("nav").height();
            $("body").animate({scrollTop : offsetTop.top-headHeight}, 400);
        };
    });



})();
