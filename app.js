/* load modules */
// node modules
var express = require('express');
var server = express();

// custom module
var apiRouter = require('./api/router');


/*
 * 1. Configuration
 */
var port = 80;
var publicPath = __dirname + "/public";
/*
*
* 1. For static server
*
*/
server.use("/", express.static(publicPath));
server.use("/api", apiRouter.router);

server.listen(port, function() {
    console.log("server is running on port " + port);
});