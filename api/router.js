var router = require('express').Router();
var dataLoader = require('../data_loader');

router.get('/', function(request, response) {
    response.send("api router");
});

router.get('/all', function(request, response){
    response.send(dataLoader.getCoinData());
});

exports.router = router;